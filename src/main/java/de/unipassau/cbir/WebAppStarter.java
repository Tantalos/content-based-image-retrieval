package de.unipassau.cbir;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebAppStarter {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(WebAppStarter.class, args);
    }
}
