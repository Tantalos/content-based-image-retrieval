package de.unipassau.cbir;

import java.io.IOException;
import java.util.List;

public interface LireFacade {


	/**
	 * Prints a list of all available indexes to the console.
	 */
    public List<String> listIndexes() throws IOException;

	/**
	 * Checks whether or not an index exists.
	 * @return true if index exists, false if not.
	 */
    public boolean exists(String index) throws IOException;

	/**
	 * Creates an index.
	 * @param index
	 * @throws IOException TODO
	 */
    public boolean createIndex(String index) throws IOException;

	/**
     * Creates an index and indexes the given documents
     *
     * @param index
     * @param entry
     * @throws IOException.
     */
    public boolean createIndex(String index, String entry) throws IOException;

	/**
	 * Deletes an index.
	 * @throws IOException TODO
	 */
    public boolean deleteIndex(String index) throws IOException;

	/**
	 * Deletes all entries in the index.
	 * @throws IOException TODO
	 */
    public boolean truncateIndex(String index) throws IOException;

	/**
	 * Inserts a new entry into an index.
	 * @param index
	 * @throws IOException TODO
	 */
    public boolean insert(String entry, String index) throws IOException;

	/**
	 * Removes an entry from an index.
	 * @param entry
	 * @throws IOException TODO
	 */
    public boolean remove(String entry, String index) throws IOException;

    /**
     * Prints a list of similar entries to the console (a parameter should
     * specify the number of the printed entries)
     *
     * @param index
     *            TODO
     * @param max
     *            TODO
     * @throws IOException
     *             TODO
     */
    public List<String> listSimilarEntries(String entry, String index, int max) throws IOException;

    /**
     * Prints a list of similar entries to the console (a parameter should
     * specify the number of the printed entries)
     *
     * @param index
     *            TODO
     * @throws IOException
     *             TODO
     */
    public List<String> listSimilarEntries(String entry, String index) throws IOException;
}
