package de.unipassau.cbir.rest;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Handler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LogEndpoint {
	
	private static final Logger log; 

	static {
		log = Logger.getLogger(LogEndpoint.class.getName());
		log.setUseParentHandlers(false);
		log.addHandler(createRemoteHandler());
	}
	
    @RequestMapping(value = "/loginfo", method = RequestMethod.POST)
    public ResponseEntity<Void> loginfo(@RequestBody String logMessage) throws UnsupportedEncodingException {
    	String message = URLDecoder.decode(logMessage, "UTF-8");
    	log.info(message);
    	
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    private static Handler createRemoteHandler() {
    	return new Handler() {
			
			@Override
			public void publish(LogRecord record) {
				System.out.println("REMOTE: " +  record.getMessage());
			}
			
			@Override
			public void flush() {
				
			}
			
			@Override
			public void close() throws SecurityException {
			}
		};
    }
}
