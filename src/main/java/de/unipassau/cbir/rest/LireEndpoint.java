package de.unipassau.cbir.rest;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.unipassau.cbir.IndexService;

@RestController
@RequestMapping("index")
public class LireEndpoint {

    @Autowired
    private IndexService indexService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<List<String>> index() {
        try {
            List<String> indexes = indexService.listIndexes();
            return new ResponseEntity<>(indexes, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> exists(@PathVariable String indexname) {
        try {
            boolean exists = indexService.exists(indexname);
            return new ResponseEntity<>(exists, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}", method = RequestMethod.POST)
    public ResponseEntity<Void> createIndex(@PathVariable String indexname) {
        try {
            if (indexService.createIndex(indexname)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteIndex(@PathVariable String indexname) {
        try {
            if (indexService.deleteIndex(indexname)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}/truncate", method = RequestMethod.GET)
    public ResponseEntity<Void> truncateIndex(@PathVariable String indexname) {
        try {
            if (indexService.truncateIndex(indexname)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}/{entryname}/**", method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@PathVariable String indexname, HttpServletRequest request) {
        try {
            String entryname = new AntPathMatcher().extractPathWithinPattern("/{indexname}/{entryname}/**",
                    request.getRequestURI());
            entryname = entryname.replaceAll("^\\+", "/");

            if (indexService.insert(entryname, indexname)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}/{entryname}/**", method = RequestMethod.DELETE)
    public ResponseEntity<Void> remove(@PathVariable String indexname, HttpServletRequest request) {
        try {
            String entryname = new AntPathMatcher().extractPathWithinPattern("/{indexname}/{entryname}/**",
                    request.getRequestURI());
            entryname = entryname.replaceAll("^\\+", "/");

            if (indexService.remove(entryname, indexname)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/{indexname}/search", method = RequestMethod.GET)
    public ResponseEntity<List<String>> listSimilarEntries(@PathVariable String indexname,
            @RequestParam("search") String search) {
        try {
            List<String> results = indexService.listSimilarEntries(search, indexname);
            return new ResponseEntity<>(results, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
