package de.unipassau.cbir;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang.Validate;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;
import net.semanticmetadata.lire.builders.DocumentBuilder;
import net.semanticmetadata.lire.builders.GlobalDocumentBuilder;
import net.semanticmetadata.lire.imageanalysis.features.global.CEDD;
import net.semanticmetadata.lire.searchers.GenericFastImageSearcher;
import net.semanticmetadata.lire.searchers.ImageSearchHits;
import net.semanticmetadata.lire.searchers.ImageSearcher;
import net.semanticmetadata.lire.utils.FileUtils;
import net.semanticmetadata.lire.utils.LuceneUtils;

@Service
@Log
public class IndexService  implements LireFacade {

    private final GlobalDocumentBuilder globalDocumentBuilder = new GlobalDocumentBuilder(CEDD.class); // CEDD.class

    private static final Path DEFAULT_INDEX_DIR = Paths.get("./index");

    public IndexService() throws IOException {
    }

    @Override
    public List<String> listIndexes() throws IOException {
		int i = 0;
        List<String> indexes = getIndexRepositories();

        if (indexes.isEmpty()) {
            System.out.println("-There are no indexes-");
        }

        for (String index : indexes) {
			StringBuilder builder = new StringBuilder();
            builder.append("Index ")
                .append(i)
                .append(" \"")
                .append(index)
                .append("\"");

			System.out.println(builder.toString());
            i++;
		}

        return indexes;
	}

    private List<String> getIndexRepositories() {
        List<String> indexName = new ArrayList<String>();
    	File indexRepo = DEFAULT_INDEX_DIR.toFile();
    	if(indexRepo.exists() && indexRepo.isDirectory()) {
        	for(File file : indexRepo.listFiles()) {
        		if(file.isDirectory()) {
        			indexName.add(file.getName());
        		}
        	}
    	}
    	return indexName;
    }

	@Override
    public boolean exists(String index) throws IOException {
		boolean exists = getIndexRepositories().contains(index);
		log.info("index \"" + index + "\" " + (exists ? "exists" : "doesn't exist"));
		return exists;
	}


	@Override
    public boolean createIndex(String index) throws IOException {
		Validate.notNull(index, "Index name can not be null");
        List<String> indexes = getIndexRepositories();

        if (indexes.contains(index)) {
            log.info("Index \"" + index + "\" does already exist");
            return false;
		}
        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexWriter iw = LuceneUtils.createIndexWriter(p.toString(), true, LuceneUtils.AnalyzerType.WhitespaceAnalyzer);

        iw.close();
        log.info("Index \"" + index + "\" was created");
        return true;
	}

	@Override
    public boolean createIndex(String index, String entry) throws IOException {
        boolean success = createIndex(index);
        insert(entry, index); // TODO success &= insert(entry, index);
        return success;
	}


	@Override
    public boolean deleteIndex(String index) throws IOException {
		Validate.notNull(index, "Index name can not be null");
        List<String> indexes = getIndexRepositories();

        if (!indexes.contains(index)) {
            log.info("Index \"" + index + "\" does not exist");
            return false;
		}
        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexWriter iw = LuceneUtils.createIndexWriter(p.toString(), false, LuceneUtils.AnalyzerType.WhitespaceAnalyzer);

		iw.deleteAll();
		iw.commit();
		iw.close();

        try {
            org.apache.commons.io.FileUtils.deleteDirectory(p.toFile());
		} catch (IOException e) {
			log.warning("System refused to delete directory of index \"" + index
					+ "\". Index was cleared but leftover files may persist.");
		}
        
        log.info("Index \"" + index + "\" was deleted");


        return true;
	}

	@Override
    public boolean truncateIndex(String index) throws IOException {
		Validate.notNull(index, "Index name can not be null");
        List<String> indexes = getIndexRepositories();

        if (!indexes.contains(index)) {
            log.info("Index \"" + index + "\" does not exist");
            return false;
        }
        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexWriter iw = LuceneUtils.createIndexWriter(p.toString(), false, LuceneUtils.AnalyzerType.WhitespaceAnalyzer);

        iw.deleteDocuments(new MatchAllDocsQuery());
		iw.commit();
        iw.close();
        
        log.info("Index \"" + index + "\" was truncated");


        return true;
	}

    @Override
    public boolean insert(String entry, String index) throws IOException {
        Validate.notNull(index, "Index name can not be null");
        Validate.notNull(entry, "Document name can not be null");
        List<String> indexes = getIndexRepositories();
        boolean success = false;

        if (!indexes.contains(index)) {
            log.info("Index \"" + index + "\" does not exist");
            return false;
        }

        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexWriter iw = LuceneUtils.createIndexWriter(p.toString(), false,
                LuceneUtils.AnalyzerType.WhitespaceAnalyzer);

        File file = new File(entry);

        if (file.isDirectory()) {
            ArrayList<String> images = FileUtils.readFileLines(file, true);
            for (String imagePath : images) {
                indexFile(imagePath, iw);
            }
            success = true;
        } else if (file.isFile()) {
            indexFile(file.toString(), iw);
            success = true;
        } else {
            System.out.println("Given entry was no valid file or directory. No index was created");
            success = false;
        }

        iw.commit();
        iw.close();
        
        log.info(success ? "entry \"" + entry + "\" was inserted to index \"" + index + "\"." :
        	"entry \"" + entry + "\" was not inserted to index \"" + index + "\".");

        return success;
    }

    private void indexFile(String imagePath, IndexWriter iw) throws IOException {
        System.out.println("Indexing " + imagePath);
        BufferedImage img = ImageIO.read(new FileInputStream(imagePath));
        Document document = globalDocumentBuilder.createDocument(img, imagePath);
        iw.addDocument(document);
    }



    @Override
    public boolean remove(String entry, String index) throws IOException {
        Validate.notNull(index, "Index name can not be null");
        Validate.notNull(entry, "Document name can not be null");
        List<String> indexes = getIndexRepositories();

        if (!indexes.contains(index)) {
            log.info("Index \"" + index + "\" does not exist");
            return false;
        }

        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexWriter iw = LuceneUtils.createIndexWriter(p.toString(), false,
                LuceneUtils.AnalyzerType.WhitespaceAnalyzer);
        iw.deleteDocuments(new Term(DocumentBuilder.FIELD_NAME_IDENTIFIER, entry));
        iw.commit();
        iw.close();
        
        log.info("entry \"" + entry + "\" was removed from index \"" + index + "\".");

        return true;
    }


	@Override
    public List<String> listSimilarEntries(String entry, String index, int max) throws IOException {
        Validate.notNull(index, "Index name can not be null");
        Validate.notNull(entry, "Document name can not be null");
        List<String> indexes = getIndexRepositories();

        List<String> similarImages = new LinkedList<>();

        if (!indexes.contains(index)) {
            log.info("Index \"" + index + "\" does not exist");
            return null;
        }

        BufferedImage img = ImageIO.read(new File(entry));

        Path p = DEFAULT_INDEX_DIR.resolve(index);
        IndexReader reader = LuceneUtils.openIndexReader(p.toString());
        ImageSearcher searcher = new GenericFastImageSearcher(max, CEDD.class);

        ImageSearchHits hits = searcher.search(img, reader);

        for (int i = 0; i < hits.length(); i++) {
            Integer docId = hits.documentID(i);
            Document foundDocument = reader.document(docId);
            String imageName = foundDocument.getValues(DocumentBuilder.FIELD_NAME_IDENTIFIER)[0];
            similarImages.add(imageName);

            // for console output only
            StringBuilder imageOutput = new StringBuilder().append("Image ").append(imageName);

            System.out.println(imageOutput.toString());
        }
        log.info("Similar Images are retrieved for index \"" + index + "\" and sample \"" + entry + "\".");

        return similarImages;
	}

    @Override
    public List<String> listSimilarEntries(String entry, String index) throws IOException {
        return listSimilarEntries(entry, index, 12);
    }

}
