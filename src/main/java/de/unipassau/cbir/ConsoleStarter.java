package de.unipassau.cbir;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleStarter {
    public static void main(String[] args) throws IOException {

        IndexService indexService = new IndexService();

        while (true) {
            System.out.println("Please enter a command");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input = br.readLine();
            String[] tokens = input.split(" ");

            if (tokens.length < 1) {
                System.out.println("There was no command specified");
                continue;
            }

            switch (tokens[0]) {
            case "list":
                if (!checkAndPrintMissingParm(tokens, 0)) {
                    continue;
                }
                indexService.listIndexes();
                break;
            case "exists":
                if (!checkAndPrintMissingParm(tokens, 1)) {
                    continue;
                }
                boolean exists = indexService.exists(tokens[1]);
                System.out.println(exists);
                break;
            case "create":
                if (!checkAndPrintMissingParm(tokens, 1)) {
                    continue;

                }

                if(isParamCount(tokens, 2)) {
                    indexService.createIndex(tokens[1], tokens[2]);
                } else if(isParamCount(tokens, 1)) {
                    indexService.createIndex(tokens[1]);
                } else {
                    assert false;
                }

                break;
            case "delete":
                if (!checkAndPrintMissingParm(tokens, 1)) {
                    continue;
                }
                indexService.deleteIndex(tokens[1]);
                break;
            case "truncate":
                if (!checkAndPrintMissingParm(tokens, 1)) {
                    continue;
                }
                indexService.truncateIndex(tokens[1]);
                break;
            case "insert":
                if (!checkAndPrintMissingParm(tokens, 2)) {
                    continue;
                }
                indexService.insert(tokens[1], tokens[2]);
                break;
            case "remove":
                if (!checkAndPrintMissingParm(tokens, 2)) {
                    continue;
                }
                indexService.remove(tokens[1], tokens[2]);
                break;
            case "listSimilar":
                if (!checkAndPrintMissingParm(tokens, 2)) {
                    continue;
                }

                if (isParamCount(tokens, 3)) {
                    if (!isInt(tokens[3])) {
                        System.out.println("The amount of similar documents must be an integer");
                        continue;
                    }
                    Integer max = Integer.parseInt(tokens[3]);
                    indexService.listSimilarEntries(tokens[1], tokens[2], max);
                } else if (isParamCount(tokens, 2)) {
                    indexService.listSimilarEntries(tokens[1], tokens[2]);
                } else {
                    assert false;
                }

                break;
            case "exit":
                System.exit(0);
                break;
            case "help":
                StringBuilder helpString = new StringBuilder();
                helpString.append("use the commands to perform lire operations\n")
                        .append("\t list \t\t\t\t lists all available indexes \n")
                        .append("\t exists <index> \t\t checks if index exists\n")
                        .append("\t create <index> [directory] \t\t creates a new index on an optional given directory \n")
                        .append("\t delete <index> \t\t deletes an index if it does exists\n")
                        .append("\t truncate <index> \t\t deletes all entries in the index\n")
                        .append("\t insert <document> <index> \t inserts a new entry to the index\n")
                        .append("\t remove <document> <index> \t removes an entry from the index\n")
                        .append("\t listSimilar <document> <index> <number> \t lists similar documents in the index (a parameter should specify the number of the printed entries)\n")
                        .append("\t exit \t\t\t\t terminates this program\n")
                        .append("\t help \t\t\t\t shows this help\n");

                System.out.println(helpString.toString());
                break;
            default:
                System.out.println("Cannot perform " + input + ". Use the defined commands");
                continue;
            }
        }

    }

    private static boolean isInt(String string) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean checkAndPrintMissingParm(Object[] array, int count) {
        if (!isParamCount(array, count)) {
            System.out.println("The command \"" + array[0].toString()
                    + "\" was not called with the expected amount of parameters " + (count));
            return false;
        }
        return true;
    }

    private static boolean isParamCount(Object[] array, int count) {
        return array.length > count;
    }
}
